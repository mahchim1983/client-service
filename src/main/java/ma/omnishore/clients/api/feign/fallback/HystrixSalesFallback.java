package ma.omnishore.clients.api.feign.fallback;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import ma.omnishore.clients.api.feign.SalesClient;
import ma.omnishore.clients.dto.SaleDto;

@Component
public class HystrixSalesFallback implements SalesClient {

	@Override
	public List<SaleDto> getClientSales(Long clientId) {
		return new ArrayList<>();
	}
}