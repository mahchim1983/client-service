package ma.omnishore.clients.config;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class KeycloakProvider {

	@SuppressWarnings("unchecked")
	public String getConnectedUser() {
		KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return kp.getKeycloakSecurityContext().getToken().getPreferredUsername();
	}
}
